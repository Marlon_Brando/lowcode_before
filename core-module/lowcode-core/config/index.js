'use strict'

const path = require('path')

module.exports = {
  dev: {

    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    proxyTable: {
      // 后台模块地址1
      '/back': {
        target: 'http://127.0.0.1:8014',
        changeOrigin: true, // 改变源
        pathRewrite: {
          '^/back': 'http://127.0.0.1:8014' // 路径重写
        }
      },
      // 后台模块地址2
      '/before': {
        target: 'http://127.0.0.1:8015',
        changeOrigin: true, // 改变源
        pathRewrite: {
          '^/before': 'http://127.0.0.1:8015' // 路径重写
        }
      }
    },

    host: '127.0.0.1', // can be overwritten by process.env.HOST
    port: 8081, // can be overwritten by process.env.PORT, if port is in use, a free one will be determined
    autoOpenBrowser: false,
    errorOverlay: true,
    notifyOnErrors: true,
    poll: false, // https://webpack.js.org/configuration/dev-server/#devserver-watchoptions-

    useEslint: true,
    showEslintErrorsInOverlay: false,

    devtool: 'cheap-module-eval-source-map',

    cacheBusting: true,

    cssSourceMap: true
  },

  build: {
    index: path.resolve(__dirname, '../dist/index.html'),

    // Paths
    assetsRoot: path.resolve(__dirname, '../dist'),
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',

    /**
     * Source Maps
     */

    productionSourceMap: true,
    // https://webpack.js.org/configuration/devtool/#production
    devtool: '#source-map',

    productionGzip: false,
    productionGzipExtensions: ['js', 'css'],

    bundleAnalyzerReport: process.env.npm_config_report
  }
}
