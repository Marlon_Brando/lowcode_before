import {
  requireData
} from '../../utils/common'

// 后端登录接口
export function login (url, data, type, item) {
  return requireData(url, data, type, item)
}
