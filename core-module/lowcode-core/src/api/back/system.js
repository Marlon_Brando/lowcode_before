import {
  requireData
} from '../../utils/common'

// 获取登录用户信息
export function getUserInfo (url, data, type, item) {
  return requireData(url, data, type, item)
}

// 获取菜单
export function getMenu (url, data, type, item) {
  return requireData(url, data, type, item)
}

// 获取用户分页列表
export function getUserPage (url, data, type, item) {
  return requireData(url, data, type, item)
}
