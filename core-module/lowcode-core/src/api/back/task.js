import { requireData } from '../../utils/common'

/**
 * 任务定义分页列表
 */
export function getTaskPageList (url, data, type, item) {
  return requireData(url, data, type, item)
}

/**
 * 任务立即执行
 */
export function runTaskNow (url, data, type, item) {
  return requireData(url, data, type, item)
}

/** 新增或者编辑 */
export function add (url, data, type, item, paramsType) {
  return requireData(url, data, type, item, paramsType)
}

/**
 * 删除
 */
export function deleteByIds (url, data, type, item, paramsType) {
  return requireData(url, data, type, item, paramsType)
}
