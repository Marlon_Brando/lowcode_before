import BaseTable from './table/table-plugin.vue'

// 注册全局的组件
export default(Vue) => {
  Vue.component('BaseTable', BaseTable)
}
