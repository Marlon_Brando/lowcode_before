import Vue from 'vue'

// element 和  VXETable 的依赖在  ld-lowcode-component 中
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import 'xe-utils'
import VXETable from 'vxe-table'
import 'vxe-table/lib/style.css'

// 低代码基础包
import lowcodeComponent from 'ld-lowcode-component'

Vue.use(lowcodeComponent)
Vue.use(ElementUI)
Vue.use(VXETable)

console.log('lowcodeComponent 导入的组件', lowcodeComponent)
