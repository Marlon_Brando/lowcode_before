// 引入 Vue
import Vue from 'vue'
import App from './App'
// 全局路由 js
import router from './router'
import Cookies from 'js-cookie'

// 引入全局的 backutils.js
import backutils from './utils/backutils'
// 注册全局的组件
import backcomponent from './components/back_component'
// I18n js
import i18n from './utils/i18n/I18n.js'
// 从外部导入的组件
import './external.js'

import requireData from './utils/common.js'

// http 请求
Vue.prototype.$requireData = requireData.requireData
// 引入全局的 backutils.js
Vue.prototype.$backutils = backutils
// 注册组件
Vue.use(backcomponent)
// 使用 js-cookie
Vue.use(Cookies)

// 挂载 vue
new Vue({
  router,
  i18n,
  render: h => h(App)
}).$mount('#app')
