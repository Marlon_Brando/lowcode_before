import Vue from 'vue'
import Router from 'vue-router'
//  模型中的路由
import ModelRouter from './model-router'

Vue.use(Router)

// core 模块的菜单
export default new Router({
  routes: [

    {
      path: '/back/',
      redirect: '/back/login'
    },

    {
      path: '/back/',
      component: () =>
        import('../views/backhome/Home.vue'),
      meta: {
        title: '自述文件'
      },
      children: [
        // 后端登录成功首页
        {
          path: '/back/dashboard',
          component: () =>
            import(
              '../views/page/Dashboard.vue'
            ),
          meta: {
            title: '系统首页'
          }
        },
        {
          path: '/back/usermanage',
          component: () =>
            import(
              '../views/system/usermanage/usermanage.vue'
            ),
          meta: {
            title: '用户管理',
            permission: true
          }
        },
        {
          path: '/back/menumanage',
          component: () =>
            import(
              '../views/system/menumanage/menumanage.vue'
            ),
          meta: {
            title: '菜单管理',
            permission: true
          }
        },
        {
          path: '/back/permissionmanage',
          component: () =>
            import(
              '../views/system/rolemanage/rolemanage.vue'
            ),
          meta: {
            title: '权限管理',
            permission: true
          }
        },

        {
          path: '/back/taskdefine',
          component: () =>
            import('../views/task/taskdefine/taskdefine.vue'),
          meta: {
            title: '任务定义'
          }
        },
        {
          path: '/back/taskdeploy',
          component: () =>
            import('../views/task/taskdeploy/taskdeploy.vue'),
          meta: {
            title: '任务部署'
          }
        },
        {
          path: '/back/taskperfromlog',
          component: () => import('../views/task/taskperfromlog.vue'),
          meta: {
            title: '任务执行日志'
          }
        },
        // 模型中的路由
        ...ModelRouter
      ]
    },
    {
      path: '/back/login',
      component: () => import('../views/login.vue'),
      meta: {
        title: '支持作者'
      }
    },
    {
      path: '/test/tree',
      component: () => import('../views/test/v-tree-test.vue'),
      meta: {
        title: '测试'
      }
    },
    {
      path: '/test',
      component: () => import('../views/test/vxetable.vue'),
      meta: {
        title: '测试'
      }
    }
  ]
})
