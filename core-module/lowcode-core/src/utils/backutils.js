import Cookies from 'js-cookie'

// 从 Cookie  中获取用户信息
function user () {
  try {
    return JSON.parse(Cookies.get('user'))
  } catch (err) {
    console.log(err)
    let user = {}
    return user
  }
}
export default{
  user,
  // 清除 Cookie 中的缓存
  clearCache () {
    Cookies.remove('user')
    Cookies.remove('token')
  }
}
