// 引入 axios
import axios from 'axios'
import QS from 'qs'
// 引入Cookie
import Cookies from 'js-cookie'
// Message 引入
import { Message } from 'element-ui'

//  后端请求地址
const domain = {
  Base_M1_URL: 'http://127.0.0.1:8014', // os_back模块地址
  Base_M2_URL: 'http://127.0.0.1:8015', // os_before地址
  Base_M3_URL: 'http://127.0.0.1:8013', // taskcenter地址
  Base_M4_URL: 'http://127.0.0.1:8012' // oscore地址
}
// 配置 axios 分模块处理请求
axios.defaults.baseM1URL = domain.Base_M1_URL
axios.defaults.baseM2URL = domain.Base_M2_URL
axios.defaults.baseM3URL = domain.Base_M3_URL
axios.defaults.baseM4URL = domain.Base_M4_URL
// 超时时间
axios.defaults.timeout = 180000

// 表单提交方式还是 JSON格式提交
var flag = true
// http request 拦截器
axios.interceptors.request.use(
  config => {
    // 注意使用的时候需要引入cookie方法，推荐js-cookie
    let token = Cookies.get('token')
    let language = Cookies.get('language') === undefined ? 'zh' : Cookies.get('language')
    // 登录接口不需要Token
    if (config.url.indexOf('/system/login') > 0 && config.url.indexOf('/system/loginlog') < 0) {
      return config
    }
    // 如果是post请求 请求体自动加上token
    if (config.method === 'post') {
      if (token) {
        console.log(config.headers)
        config.headers = {
          'Content-Type': flag ? 'application/json' : 'application/x-www-form-urlencoded',
          // 后端生成的Token,携带Token后请求变成非简单请求
          Authorization: token,
          'language': language
        }
      }
    } else if (config.method === 'get') {
      config.headers = {}
      if (token) {
        config.headers = {
          'Content-Type': flag ? 'application/json' : 'application/x-www-form-urlencoded',
          Authorization: token,
          'language': language
        }
      }
    }
    return config
  },
  error => {
    console.log(error)
  }
)

// 2->对get请求传递过来的参数处理
function paramsToUrl (url, params) {
  if (!params) return url
  for (var key in params) {
    if (params[key] && params[key] !== 'undefined') {
      if (url.indexOf('?') !== -1) {
        url += '&' + '' + key + '=' + params[key] || '' + ''
      } else {
        url += '?' + '' + key + '=' + params[key] || '' + ''
      }
    }
  }
  return url
}

// 3->在common.js中封装公用方法-----封装请求的方法
export function requireData (url, params, type, item, paramsType) {
  // 请求参数是表单还是 json 格式
  flag = true
  if (paramsType) {
    flag = paramsType
  }
  if (!url) return false
  switch (item) {
    case 'back':
      url = axios.defaults.baseM1URL + url
      break
    case 'before':
      url = axios.defaults.baseM2URL + url
      break
    case 'taskcenter':
      url = axios.defaults.baseM3URL + url
      break
    case 'core':
      url = axios.defaults.baseM4URL + url
      break
  }
  if (type === 'get') {
    url = paramsToUrl(url, params)
    return new Promise((resolve, reject) => {
      axios
        .get(url, params)
        .then(res => {
          resolve(res.data)
        })
        .catch(err => {
          reject(err)
        })
    })
  } else if (type === 'post') {
    // json格式请求头
    const headerJSON = {
      'Content-Type': 'application/json'
    }
    // FormData格式请求头
    const headerFormData = {
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
    }
    return new Promise((resolve, reject) => {
      axios.post(url, flag ? JSON.stringify(params) : QS.stringify(params), {
        headers: flag ? headerJSON : headerFormData
      }).then(res => {
        resolve(res.data)
      }).catch(error => {
        if (error.response && error.response.data) {
          // 返回 response 中的 data
          resolve(error.response.data)
        } else {
          Message.error(error)
        }
      })
    })
  }
}

export default{
  requireData
}
