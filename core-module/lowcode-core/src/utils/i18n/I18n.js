// I18n
import VueI18n from 'vue-i18n'
import Vue from 'vue'
import locale from 'element-ui/lib/locale'

// 引入 elementui 的多语言
import enLocale from 'element-ui/lib/locale/lang/en'
import zhLocale from 'element-ui/lib/locale/lang/zh-CN'

// 引入自己定义的 I18n 文件
import myI18nEn from './i18n-en-US.json'
import myI18nZh from './i18n-zh-CN.json'

// 注册 vue-i18n
Vue.use(VueI18n)

// 默认中文
const lang = 'zh'
const i18n = new VueI18n({
  locale: lang,
  messages: {
    zh: Object.assign(zhLocale, myI18nZh),
    en: Object.assign(enLocale, myI18nEn)
  }
})

locale.i18n((key, value) => i18n.t(key, value))
export default i18n
