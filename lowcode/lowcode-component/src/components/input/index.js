// 导入组件，组件必须声明 name
import VinputSelectTree  from './v-input-select-tree.vue'

// 为组件提供 install 安装方法，供按需引入
VinputSelectTree.install = function (Vue) {
	console.log(VinputSelectTree)
  Vue.component(VinputSelectTree.name, VinputSelectTree)
}

// 默认导出组件
export default VinputSelectTree
