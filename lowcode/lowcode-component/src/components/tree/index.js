// 导入组件，组件必须声明 name
import VelTree  from './v-el-tree.vue'

// 为组件提供 install 安装方法，供按需引入
VelTree.install = function (Vue) {
  Vue.component(VelTree.name, VelTree)
}

// 默认导出组件
export default VelTree
