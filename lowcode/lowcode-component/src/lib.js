// 树
import VelTree from './components/tree';
// input 选择树组件
import VinputSelectTree from './components/input'


// 所有组件列表
const components = [
  VelTree,
  VinputSelectTree
]

// 定义install方法，接收Vue作为参数
const install = Vue => {
  if (install.installed) { return }
  install.installed = true
  // vue.component 注册组件
  components.map(component => { Vue.component(component.name, component) })
}
// 判断是否是直接引入文件
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}

export default{
  install,
  components
}
