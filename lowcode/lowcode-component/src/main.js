import Vue from 'vue'
import App from './App'
import router from './router'

import vxetable from './components/vxetable'
import elementui from './components/elementui'

// 全局注册组件测试
// import components from './lib.js'
// Vue.use(components)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
