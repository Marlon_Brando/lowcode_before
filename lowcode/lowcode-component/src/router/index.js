import Vue from 'vue'
import Router from 'vue-router'

import eltree from '@/components/tree/v-el-tree'
import testeltree from '@/components/demo/test-tree.vue'
import testvxetable from '@/components/demo/test-vxe-table.vue'

import inputselecttree from '@/components/demo/test-v-input-select-tree.vue'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'testeltree',
      component: testeltree
    },
    {
      path: '/tree',
      name: 'eltree',
      component: eltree
    },
    {
      path: '/testvxetable',
      name: 'testvxetable',
      component: testvxetable
    },
    {
      path: '/inputselecttree',
      name: 'inputselecttree',
      component: inputselecttree
    }
  ]
})
