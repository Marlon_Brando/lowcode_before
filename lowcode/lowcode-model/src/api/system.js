import {
  requireData
} from '../utils/common.js'

// 获取菜单
export function getMenu (url, data, type, item) {
  return requireData(url, data, type, item)
}

export default {
  getMenu
}