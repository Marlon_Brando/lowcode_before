
import datasource from './components/datasource/datasource.vue'
import model from './components/model/model.vue'

/*配置路由*/
const routes=[
    {
			path:"/back/model/model",component:model,	meta: {title:"数据库建模",	permission: true}
		},
    {
			path:"/back/model/datasource",component:datasource,	meta: {title:"数据源",	permission: true}
		}
]

/*导出路由模块*/
// export default routes


import getMenu from './api/system.js'

// 所有的接口
const apis =[
	getMenu
]

export default{
	/*导出路由模块*/
	routes,
	apis
}